vowels = ['а', 'е', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я']
consonants = ['б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ']
perfective_gerund = [('ав', 1), ('авши', 1), ('авшись', 1), ('яв', 1), ('явши', 1), ('явшись', 1), ('ив', 0), ('ивши', 0), ('ившись', 0), ('ыв', 0), ('ывши', 0), ('ывшись', 0)]
adjective = [('ее', 0), ('ие', 0), ('ые', 0), ('ое', 0), ('ими', 0), ('ыми', 0), ('ей', 0), ('ий', 0), ('ый', 0), ('ой', 0), ('ем', 0), ('им', 0), ('ым', 0), ('ом', 0), ('его', 0), ('ого', 0), ('ему', 0),
             ('ому', 0), ('их', 0), ('ых', 0), ('ую', 0), ('юю', 0), ('ая', 0), ('яя', 0), ('ою', 0), ('ею', 0)]
participle = [('аем', 1), ('анн', 1), ('авш', 1), ('ающ', 1), ('ащ', 1), ('яем', 1), ('янн', 1), ('явш', 1), ('яющ', 1), ('ящ', 1), ('ивш', 0), ('ывш', 0), ('ующ', 0)]
reflexive = [('ся', 0), ('сь', 0)]
verb = [('ала', 1), ('ана', 1), ('аете', 1), ('айте', 1), ('али', 1), ('ай', 1), ('ал', 1), ('аем', 1), ('ан', 1), ('ало', 1), ('ано', 1), ('ает', 1), ('ают', 1), ('аны', 1), ('ать', 1), ('аешь', 1),
        ('анно', 1), ('яла', 1), ('яна', 1), ('яете', 1), ('яйте', 1), ('яли', 1), ('яй', 1), ('ял', 1), ('яем', 1), ('ян', 1), ('яло', 1), ('яно', 1), ('яет', 1), ('яют', 1), ('яны', 1), ('ять', 1),
        ('яешь', 1), ('янно', 1), ('ила', 0), ('ыла', 0), ('ена', 0), ('ейте', 0), ('уйте', 0), ('ите', 0), ('или', 0), ('ыли', 0), ('ей', 0), ('уй', 0), ('ил', 0), ('ыл', 0), ('им', 0), ('ым', 0),
        ('ен', 0), ('ило', 0), ('ыло', 0), ('ено', 0), ('ят', 0), ('ует', 0), ('уют', 0), ('ит', 0), ('ыт', 0), ('ены', 0), ('ить', 0), ('ыть', 0), ('ишь', 0), ('ую', 0), ('ю', 0)]
noun = [('а', 0), ('ев', 0), ('ов', 0), ('ие', 0), ('ье', 0), ('е', 0), ('иями', 0), ('ями', 0), ('ами', 0), ('еи', 0), ('ии', 0), ('и', 0), ('ией', 0), ('ей', 0), ('ой', 0), ('ий', 0), ('й', 0), ('иям', 0),
        ('ям', 0), ('ием', 0), ('ем', 0), ('ам', 0), ('ом', 0), ('о', 0), ('у', 0), ('ах', 0), ('иях', 0), ('ях', 0), ('ы', 0), ('ь', 0), ('ию', 0), ('ью', 0), ('ю', 0), ('ия', 0), ('ья', 0), ('я', 0)]
superlative = [('ейш', 0), ('ейше', 0)]
derivational = [('ост', 0), ('ость', 0)]

adjectival = adjective.copy()
for participle_ending in participle:
    for adjective_ending in adjective:
        adjectival.append((participle_ending[0] + adjective_ending[0], participle_ending[1]))


def __rv_area(word: str):
    for i in range(len(word)):
        if word[i].lower() in vowels:
            return word[i + 1:]
    return ''


def __r_area(word: str):
    for i in range(len(word) - 1):
        if word[i].lower() in vowels and word[i + 1] in consonants:
            return word[i + 2:]
    return ''


def __remove_ending(word: str, endings: list, start_index: int):
    for i in range(start_index, len(word)):
        for ending in endings:
            if word[i:] == ending[0]:
                return word[:(i + ending[1])]
    return word


def stem(word: str):
    processed_word = word

    first_index_after_rv_area = len(processed_word) - len(__rv_area(processed_word))
    step_processed_word = __remove_ending(processed_word, perfective_gerund, first_index_after_rv_area)

    if step_processed_word == processed_word:
        processed_word = __remove_ending(processed_word, reflexive, first_index_after_rv_area)
        step_processed_word = __remove_ending(processed_word, adjectival, first_index_after_rv_area)
        if processed_word == step_processed_word:
            step_processed_word = __remove_ending(step_processed_word, verb, first_index_after_rv_area)
            if step_processed_word == processed_word:
                step_processed_word = __remove_ending(step_processed_word, noun, first_index_after_rv_area)

    processed_word = step_processed_word

    if processed_word[first_index_after_rv_area:].endswith('и'):
        processed_word = processed_word[:-1]

    first_index_after_r2_area = len(processed_word) - len(__r_area(__r_area(processed_word)))
    processed_word = __remove_ending(processed_word, derivational, first_index_after_r2_area)

    if processed_word[first_index_after_rv_area:].endswith('нн'):
        return processed_word[:-1]

    superlative_processed_word = __remove_ending(processed_word, superlative, first_index_after_rv_area)

    if processed_word != superlative_processed_word:
        processed_word = superlative_processed_word
        if processed_word[first_index_after_rv_area:].endswith('нн'):
            processed_word = processed_word[:-1]
        return processed_word

    if processed_word[first_index_after_rv_area:].endswith('ь'):
        return processed_word[:-1]

    return processed_word
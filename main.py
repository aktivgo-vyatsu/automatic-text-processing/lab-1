import re

from stemmer import *

if __name__ == '__main__':
    with open(input('Введите путь до входного файла: '), 'r') as file:
        words =  [word for word in re.split(r'[\s\d,.!?\n\r()\[\]-]+', file.read())
                  if word is not None and len(word) != 0]
        words = [word.lower() for word in words]

    stemmed_words = [stem(word) for word in words]
    stemmed_words.sort()

    with (open(input('Введите путь до выходного файла: '), 'w')) as file:
        file.write('\n'.join(stemmed_words))